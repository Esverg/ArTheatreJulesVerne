﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Qcm : MonoBehaviour
{
    public TextAsset textAssetData;

    public GameObject Go_QuestionTheme;
    public GameObject Go_Answer1;
    public GameObject Go_Answer2;
    public GameObject Go_Answer3;
    public GameObject Go_Answer4;

    [HideInInspector]
    public List<GameObject> all_Go_Answer;

    public int currentQuestion;

    [System.Serializable]
    public class Question
    {
        public string QuestionTheme;
        public string goodAnswer;
        public string badAnswer1;
        public string badAnswer2;
        public string badAnswer3;
    }

    [System.Serializable]
    public class QuestionList
    {
        public Question[] question;
    }

    public QuestionList myQuestionList = new QuestionList();

    // Start is called before the first frame update
    void Start()
    {
        ReadCSV();

        all_Go_Answer.Add(Go_Answer1);
        all_Go_Answer.Add(Go_Answer2);
        all_Go_Answer.Add(Go_Answer3);
        all_Go_Answer.Add(Go_Answer4);
    }

    void ReadCSV()
    {
        string[] data = textAssetData.text.Split(new string[] { ";", "\n" }, StringSplitOptions.None);

        int tableSize = data.Length / 5 - 1;
        myQuestionList.question = new Question[tableSize];

        for(int i = 0; i < tableSize; i++)
        {
            myQuestionList.question[i] = new Question();
            myQuestionList.question[i].QuestionTheme = data[5 * (i + 1)];
            myQuestionList.question[i].goodAnswer = data[5 * (i + 1) + 1];
            myQuestionList.question[i].badAnswer1 = data[5 * (i + 1) + 2];
            myQuestionList.question[i].badAnswer2 = data[5 * (i + 1) + 3];
            myQuestionList.question[i].badAnswer3 = data[5 * (i + 1) + 4];
        }

        ChangeQuestion();
    }

    void ChangeQuestion()
    {
        Go_QuestionTheme.GetComponentInChildren<Text>().text = myQuestionList.question[currentQuestion].QuestionTheme;

        List<string> allAnswers = new List<string>();
        allAnswers.Add(myQuestionList.question[currentQuestion].goodAnswer);
        allAnswers.Add(myQuestionList.question[currentQuestion].badAnswer1);
        allAnswers.Add(myQuestionList.question[currentQuestion].badAnswer2);
        allAnswers.Add(myQuestionList.question[currentQuestion].badAnswer3);

        int idRandom = UnityEngine.Random.Range(0, allAnswers.Count);
        Go_Answer1.GetComponentInChildren<Text>().text = allAnswers[idRandom];
        Go_Answer1.GetComponent<Image>().color = Color.white;
        allAnswers.RemoveAt(idRandom);

        idRandom = UnityEngine.Random.Range(0, allAnswers.Count);
        Go_Answer2.GetComponentInChildren<Text>().text = allAnswers[idRandom];
        Go_Answer2.GetComponent<Image>().color = Color.white;
        allAnswers.RemoveAt(idRandom);

        idRandom = UnityEngine.Random.Range(0, allAnswers.Count);
        Go_Answer3.GetComponentInChildren<Text>().text = allAnswers[idRandom];
        Go_Answer3.GetComponent<Image>().color = Color.white;
        allAnswers.RemoveAt(idRandom);

        idRandom = UnityEngine.Random.Range(0, allAnswers.Count);
        Go_Answer4.GetComponentInChildren<Text>().text = allAnswers[idRandom];
        Go_Answer4.GetComponent<Image>().color = Color.white;
        allAnswers.RemoveAt(idRandom);
    }

    public void CheckAnswer(GameObject buttonPressed)
    {
        if(buttonPressed.GetComponentInChildren<Text>().text == myQuestionList.question[currentQuestion].goodAnswer)
        {
            buttonPressed.GetComponent<Image>().color = Color.green;
        }
        else
        {
            buttonPressed.GetComponent<Image>().color = Color.red;

            foreach (GameObject btnAnswer in all_Go_Answer)
            {
                if(btnAnswer.GetComponentInChildren<Text>().text == myQuestionList.question[currentQuestion].goodAnswer)
                {
                    btnAnswer.GetComponent<Image>().color = Color.green;
                }
            }
        }

        BtnInteractableOrNot();
        StartCoroutine(WaitingForNewQuestion());
    }

    public void BtnInteractableOrNot()
    {
        if (all_Go_Answer[0].GetComponent<Button>().interactable)
        {
            foreach (GameObject btnAnswer in all_Go_Answer)
            {
                btnAnswer.GetComponent<Button>().interactable = false;
                Debug.Log("allfalse");
            }
        }
        else
        {
            foreach (GameObject btnAnswer in all_Go_Answer)
            {
                btnAnswer.GetComponent<Button>().interactable = true;
            }
        }
    }

    public IEnumerator WaitingForNewQuestion()
    {
        currentQuestion++;

        yield return new WaitForSeconds(2f);
        ChangeQuestion();
        BtnInteractableOrNot();
    }
}

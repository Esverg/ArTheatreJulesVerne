﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Ce script est utilisé pour changer l'histoire jouée dans le theatre

public class CS_ChooseStory : MonoBehaviour
{
    public GameObject[] allStories;

    public string[] allStoriesNames;

    public Animator ac_Curtains;

    public Text storyNameText;

    //Fonction de lancement de la coroutine pour changer l'histoire (à attribuer sur l'interface dans l'appli)
    public void Launch_ChangeStory(int IdStory)
    {
        StartCoroutine(ChangeStory(IdStory));
    }

    //Coroutine de changement d'histoire
    public IEnumerator ChangeStory(int IdStory)
    {
        //Si les rideaux sont ouverts
        if (ac_Curtains.GetBool("Closing") == false && ac_Curtains.GetBool("Opening") == true)
        {
            //Fermeture des rideaux
            ac_Curtains.SetBool("Opening", false);
            ac_Curtains.SetBool("Closing", true);

            //Attendre 4 secondes
            yield return new WaitForSeconds(4f);
        }

        //Changement de l'histoire
        foreach (GameObject story in allStories)
        {
            story.SetActive(false);
        }
        allStories[IdStory].SetActive(true);

        //Ouverture des rideaux
        ac_Curtains.SetBool("Closing", false);
        ac_Curtains.SetBool("Opening", true);

        StoryNameAppear(IdStory);
    }

    public void StoryNameAppear(int idStory)
    {
        storyNameText.text = allStoriesNames[idStory];
        storyNameText.gameObject.GetComponent<Animator>().Play(0);
    }
}
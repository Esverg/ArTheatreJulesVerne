﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

/*
 * switch avec un enum pour les types d'animations 
 * direction / type de déplacement / vitesse / delai décalé (par rapport au debut)  / repawn / delai de respawn 
 * drop donw 
 * 
 * 
 * est ce need world u local ? 
 * Curve ?
 * 
 * EDITOR MODDDDE
 * 
 */


public class cs_animGlobal : MonoBehaviour
{
    #region Variables

    #region base

    private Vector3 direction;
    private float speed = 1f;
	private bool ediBoolDelay;
    private float delay;
	private bool ediBoolLoop;
	private bool ediBoolPath = false;
	[HideInInspector]
	public float smoothPath;

    #endregion

    #region enum
    public enum typeAnim 
    { 
        Move,
		Rotate
    }

	public enum typeMove
    {
		Linear, 
		Path
    }

	[HideInInspector]
	public enum typeLoop
    {
        Loop,
        PingPong
    }

	public enum easeType
	{
		easeInQuad,
		easeOutQuad,
		easeInOutQuad,
		easeInCubic,
		easeOutCubic,
		easeInOutCubic,
		easeInQuart,
		easeOutQuart,
		easeInOutQuart,
		easeInQuint,
		easeOutQuint,
		easeInOutQuint,
		easeInSine,
		easeOutSine,
		easeInOutSine,
		easeInExpo,
		easeOutExpo,
		easeInOutExpo,
		easeInCirc,
		easeOutCirc,
		easeInOutCirc,
		linear,
		spring,
		/* GFX47 MOD START */
		//bounce,
		easeInBounce,
		easeOutBounce,
		easeInOutBounce,
		/* GFX47 MOD END */
		easeInBack,
		easeOutBack,
		easeInOutBack,
		/* GFX47 MOD START */
		//elastic,
		easeInElastic,
		easeOutElastic,
		easeInOutElastic,
		/* GFX47 MOD END */
		punch
	}
    #endregion

    #region affectation des enums

	[HideInInspector]
    public typeLoop tlvalue;
	[HideInInspector]
	public typeMove tmvalue;
	[HideInInspector]
	public typeAnim tavalue;
	[HideInInspector]
	public easeType etvalue;
	[HideInInspector]
	public List<Transform> listPath;



	#endregion

	#region chosenOne

	[HideInInspector]
	public typeAnim taChosen;
	[HideInInspector]
	public typeMove tmChosen;
	[HideInInspector]
	public typeLoop tlChosen;
	[HideInInspector]
	public Vector3 destinationChosen;
	[HideInInspector]
	public float speedChosen;
	[HideInInspector]
	public float delayChosen;
	[HideInInspector]
	public easeType etChosen;
	[HideInInspector]
	public List<Transform> listPathChosen;
	[HideInInspector]
	public float smoothPathChosen;



	#endregion

	#endregion

	/// <summary>
	/// Récupération des paramètres 
	/// </summary>
	public void RecupParam()
	{
		Debug.Log("recupération parametres ");
		taChosen = tavalue;
		tmChosen = tmvalue;
		tlChosen = tlvalue;
		destinationChosen = direction;
		speedChosen = speed;
		delayChosen = delay;
		listPathChosen = listPath;
		etChosen = etvalue;
		smoothPathChosen = smoothPath;


	}


    private void Start()
    {
		//move 

		//récupération des variables
		tavalue = taChosen;
		tmvalue = tmChosen;
		tlvalue = tlChosen;
		direction = destinationChosen;
		speed = speedChosen;
		delay = delayChosen;
		listPath = listPathChosen;
		etChosen = etvalue;
		smoothPathChosen = smoothPath;


		//Movement selon les paramètres actuel
		switch (tavalue)
        {
            case typeAnim.Move:

                if(tmChosen == typeMove.Linear)
                {
					iTween.MoveTo(gameObject,
					 iTween.Hash("x", direction.x,
					  "y", direction.y,
					  "z", direction.z,
					  "speed", speed,
					  "easeType", etvalue, 
					  "loopType", tlvalue,
					  "delay", delay));
				}
						
				if(tmChosen == typeMove.Path)
                {
					iTween.MoveTo(gameObject,
					 iTween.Hash("x", direction.x,
					  "y", direction.y,
					  "z", direction.z,
					  "path", listPath.ToArray(),
					"movetopath", true,
					"lookahead",smoothPath,
					  "speed", speed,
					  "easeType", etvalue,
					  "loopType", tlvalue,
					  "delay", delay));
				}

				break;


            case typeAnim.Rotate:
				iTween.RotateTo(gameObject,
				iTween.Hash("x", direction.x,
				 "y", direction.y,
				 "z", direction.z,
				 "speed", speed,
				  "easeType", etvalue,
				 "loopType", tlvalue,
				 "delay", delay));


				break;
            default:
                break;
        } 

		Debug.Log("" + tavalue );
    }



    #region Editor
#if UNITY_EDITOR

    [CustomEditor(typeof(cs_animGlobal))]
    public class AnimGlobalClassEditor : Editor
    {
		public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            cs_animGlobal animGl = (cs_animGlobal)target;
           
            MessageInfo(animGl);

            ParamMove(animGl);

            ModeLoop(animGl);

            ButtonEditor(animGl);

        }

		/// <summary>
		/// Info et warning en haut de l'inspecteur
		/// </summary>
		/// <param name="animGl"></param>
        private static void MessageInfo(cs_animGlobal animGl)
        {
			switch (animGl.tavalue)
			{
				case typeAnim.Move:

					if (animGl.tmChosen == typeMove.Linear)
					{
						EditorGUILayout.HelpBox("\n" + "PARAMETRES ENREGISTRES POUR CET OBJET \n \n" +
                            "Direction : " + animGl.destinationChosen
                            + "\nType d'animation : " + animGl.taChosen
							+ "\nType ease : " + animGl.etChosen
							+ "\nType de mouvement : " + animGl.tmChosen
                            + "\nType de loop : " + animGl.tlChosen
                            + "\nVitesse : " + animGl.speedChosen
                            + "\nDelay : " + animGl.delay
                            + "\n"
                            , MessageType.Warning,
                            true);
					}

					if (animGl.tmChosen == typeMove.Path)
					{
						EditorGUILayout.HelpBox("\n" + "PARAMETRES ENREGISTRES POUR CET OBJET \n \n" +
							"Direction : " + animGl.destinationChosen
							+ "\nType d'animation : " + animGl.taChosen
							+ "\nType ease : " + animGl.etChosen
							+ "\nType de mouvement : " + animGl.tmChosen
							+ "\nSmooth Path (%) : " + animGl.smoothPathChosen
							+ "\nType de loop : " + animGl.tlChosen
							+ "\nVitesse : " + animGl.speedChosen
							+ "\nDelay : " + animGl.delay
							+ "\n"
							, MessageType.Warning,
							true);
					}

					break;


				case typeAnim.Rotate:

					EditorGUILayout.HelpBox("\n" + "PARAMETRES ENREGISTRES POUR CET OBJET \n \n" +
							"Direction : " + animGl.destinationChosen
							+ "\nType d'animation : " + animGl.taChosen
							+ "\nType ease : " + animGl.etChosen
							+ "\nType de loop : " + animGl.tlChosen
							+ "\nVitesse : " + animGl.speedChosen
							+ "\nDelay : " + animGl.delay
							+ "\n"
							, MessageType.Warning,
							true);

					break;

				default:
					break;
			}


            EditorGUILayout.HelpBox("Changement des valeurs ci dessous (n'oubliez le bouton SAUVEGARDE)" +
				"\n Pour easyType https://easings.net/ " +
				"\n La vitesse doit être différentes de 0 !", MessageType.Info, true);
        }

        public void Lollol(cs_animGlobal animGl)
        {
			Debug.Log("fuckkkkkkkkkkkkkkk avant les 5 secondes ");


			animGl.tavalue = animGl.taChosen;
			animGl.tmvalue = animGl.tmChosen;
			animGl.tlvalue = animGl.tlChosen;
			animGl.direction = animGl.destinationChosen;
			animGl.speed = animGl.speedChosen;
			animGl.delay = animGl.delayChosen;

			Debug.Log("fuckkkkkkkkkkkkkkk");
		}


		private static void ModeLoop(cs_animGlobal animGl)
        {
			
            animGl.ediBoolLoop = EditorGUILayout.Toggle("Loop ?", animGl.ediBoolLoop);

            if (animGl.ediBoolLoop)
            {
				animGl.tlvalue =(typeLoop)EditorGUILayout.EnumPopup("LoopType",animGl.tlvalue );

			}

		}

       
			//tlvalue = (typeLoop)EditorGUILayout.EnumPopup("enum Type Loop", tlvalue);
			

		/// <summary>
		/// Direction / delay
		/// </summary>
		/// <param name="animGl"></param>
		private static void ParamMove(cs_animGlobal animGl)
        {
			//type d'animation
			animGl.tavalue =  (typeAnim)EditorGUILayout.EnumPopup("Type d'animation", animGl.tavalue);

			// si movement choix du type de mouvement
			if(animGl.tavalue == typeAnim.Move)
            {
				animGl.tmvalue = (typeMove)EditorGUILayout.EnumPopup("Type de mouvement", animGl.tmvalue);
            }

			EditorGUILayout.Space();

			// si path choix du path
			if (animGl.tmvalue == typeMove.Path && animGl.tavalue == typeAnim.Move)
            {
				animGl.smoothPath = EditorGUILayout.Slider("Smooth Path", animGl.smoothPath, 0f, 1f);

				animGl.ediBoolPath = EditorGUILayout.Foldout(animGl.ediBoolPath, "Path", true);

				if (animGl.ediBoolPath)
				{
					EditorGUI.indentLevel++;

					List<Transform> list = animGl.listPath;
					int size = Mathf.Max(0, EditorGUILayout.IntField("Size", list.Count));

					while (size > list.Count)
					{
						list.Add(null);
					}

					while (size < list.Count)
					{
						list.RemoveAt(list.Count - 1);
					}

					for (int i = 0; i < list.Count; i++)
					{
						list[i] = EditorGUILayout.ObjectField("GO " + i, list[i], typeof(Transform), true) as Transform;
					}

					EditorGUI.indentLevel--;
				}
			}

			EditorGUILayout.Space();

			//type ease 
			animGl.etvalue = (easeType)EditorGUILayout.EnumPopup("Type ease", animGl.etvalue);

			EditorGUILayout.Space();

		    //direction
			if(animGl.tmvalue != typeMove.Path)
            {
				animGl.direction = EditorGUILayout.Vector3Field("Destination", animGl.direction);
			}
			
			EditorGUILayout.Space();

			//animGl.es

			EditorGUILayout.Space();

			//vitesse
			animGl.speed = EditorGUILayout.FloatField("Vitesse", animGl.speed);

            EditorGUILayout.Space();

			//delai ou pas si oui combien
            animGl.ediBoolDelay = EditorGUILayout.Toggle("Delai ?", animGl.ediBoolDelay);

			//delay
			if (animGl.ediBoolDelay)
			{
				animGl.delay = EditorGUILayout.FloatField("Delay", animGl.delay);
            }
            else 
			{
				animGl.delay = 0f;
            }

		}


        private void ButtonEditor(cs_animGlobal animGl)
        {
            //lancement de l'anim avec tt les param
            if (GUILayout.Button("Sauvegarde"))
            {
				animGl.RecupParam();

				Debug.Log("name : " + animGl.name + " \n boolLoop :" + animGl.ediBoolLoop + "\n type loop " + animGl.tlvalue + "\n Direction" + animGl.direction);

            }
			
        }
	}
#endif
#endregion





}